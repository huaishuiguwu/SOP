package com.gitee.sop.adminserver.mapper;

import com.gitee.fastmybatis.core.mapper.CrudMapper;

import com.gitee.sop.adminserver.entity.IsvInfo;


/**
 * @author tanghc
 */
public interface IsvInfoMapper extends CrudMapper<IsvInfo, Long> {
}
